package hust.soict.globalict.lab01;

import java.util.Scanner;

public class CalculateTwoNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter number 1: ");
        double number1 = Double.valueOf(scanner.nextLine());

        System.out.print("Enter number 2: ");
        double number2 = Double.valueOf(scanner.nextLine());

        System.out.println(number1 + " + " + number2 + " = " + (number1 + number2));
        System.out.println(number1 + " - " + number2 + " = " + (number1 - number2));
        System.out.println(number1 + " * " + number2 + " = " + (number1 * number2));
        if (number2 == 0) {
            System.out.println("Cannot divide by zero!");
        } else {
            System.out.println(number1 + " / " + number2 + " = " + (number1 / number2));
        }
    }
}
