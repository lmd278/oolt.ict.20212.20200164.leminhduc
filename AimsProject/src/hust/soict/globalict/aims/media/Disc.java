package hust.soict.globalict.aims.media;

public class Disc extends Media {
    private String director;
    private int length;

    public Disc() {
        super();
        this.director = "Unknown";
        this.length = 0;
    }

    public Disc(String title, String category, float cost) {
        super(title, category, cost);
        this.director = "Unknown";
        this.length = 0;
    }

    public Disc(String title, String category, String director, int length, float cost) {
        super(title, category, cost);
        this.director = director;
        this.length = length;
    }

    public String getDirector() {
        return director;
    }

    public int getLength() {
        return length;
    }

    @Override
    public String toString() {
        return getTitle() + " - " + getCategory() + " - " + getDirector() + " - " + getLength() + ": " + getCost() + "$";
    }
}
