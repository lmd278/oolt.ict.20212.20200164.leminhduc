package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable {
    private String artist;
    private ArrayList<Track> tracks;

    public CompactDisc() {
        super();
        this.artist = "Unknown";
        this.tracks = new ArrayList<>();
    }

    public CompactDisc(String title, String category, float cost) {
        super(title, category, cost);
        this.artist = "Unknown";
        this.tracks = new ArrayList<>();
    }

    public CompactDisc(String title, String category, String artist, ArrayList<Track> tracks, float cost) {
        super(title, category, "", 0, cost);
        this.artist = artist;
        this.tracks = tracks;
    }

    public void addTrack(Track input) {
        // Check if existed
        if (tracks.contains(input)) {
            System.out.println("The input track is already in the list!");
            return;
        }

        tracks.add(input);
    }

    public void removeTrack(Track input) {
        // Check if existed
        if (tracks.contains(input)) {
            tracks.remove(input);
            return;
        }
    }

    public int getLength() {
        int sum = 0;
        for (Track track : tracks) {
            sum += track.getLength();
        }

        return sum;
    }

    public String getArtist() {
        return artist;
    }

    @Override
    public String toString() {
        return getTitle() + " - " + getCategory() + " - " + getArtist() + " - " + getLength() + ": " + getCost() + "$";
    }

    @Override
    public void play() {
        System.out.println("Playing CD of artist: " + this.getArtist());
        System.out.println("CD's length: " + this.getLength());

        // Play tracks in CD
        for (Track track : tracks) {
            track.play();
        }
    }

    @Override
    public int compareTo(Object obj) {
        CompactDisc other = (CompactDisc) obj;
        if (tracks.size() != other.tracks.size())
            return tracks.size() - other.tracks.size();
        
        return getLength() - other.getLength();
    }
}
