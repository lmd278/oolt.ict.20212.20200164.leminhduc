package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Book extends Media {
    private String content;
    private List<String> authors;
    private List<String> contentTokens;
    private Map<String,Integer> wordFrequency;

    public Book(String title) {
        super(title);
        this.authors = new ArrayList<>();
        this.content = "";
    }

    public Book(String title, String category) {
        super(title, category);
        this.authors = new ArrayList<>();
        this.content = "";
    }

    public Book(String title, String category, float cost) {
        super(title, category, cost);
        this.authors = new ArrayList<>();
        this.content = "";
    }

    public Book(String title, String category, List<String> authors) {
        super(title, category);
        this.authors = authors;
        this.content = "";
    }

    public void addAuthor(String authorName) {
        if (!authors.contains(authorName)) {
            authors.add(authorName);
        }
    }

    public void removeAuthor(String authorName) {
        if (authors.contains(authorName)) {
            authors.remove(authorName);
        }
    }

    public void processContent() {
        wordFrequency = new TreeMap<>();
        String[] arrayTokens = content.split("\\W+");
        for (String token : arrayTokens) {
            wordFrequency.putIfAbsent(token, 0);
            wordFrequency.put(token, (wordFrequency.get(token) + 1));
        }

        contentTokens = new ArrayList<String>(wordFrequency.keySet());
    }

    public String getBasicInfo() {
        return super.toString();
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
        this.processContent();
    }

    @Override
    public String toString() {
        String result = super.toString();
        result += "\n+ Content: " + content;
        result += "\n+ TokenList: " + contentTokens;
        result += "\n+ Frequency: " + wordFrequency;

        return result;
    }
}
