package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable {
    public DigitalVideoDisc() {
        super();
    }

    public DigitalVideoDisc(String title, String category, float cost) {
        super(title, category, cost);
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super(title, category, director, length, cost);
    }

    public boolean search(String title) {
        String[] tokens = title.split(" ");
        for (String token : tokens) {
            if (!this.getTitle().contains(token)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD's length: " + this.getLength());
    }

    @Override
    public int compareTo(Object obj) {
        Media other = (DigitalVideoDisc) obj;
        if (getCost() < other.getCost())
            return -1;
        if (getCost() > other.getCost())
            return 1;
        return 0;
    }
}
