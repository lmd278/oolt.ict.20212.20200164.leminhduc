package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MemoryDaemon;

import java.util.ArrayList;
import java.util.Scanner;

public class Aims {
    public static void main(String[] args) {
        MemoryDaemon md = new MemoryDaemon();
        Thread t1 = new Thread(md);
        t1.start();

        Scanner scanner = new Scanner(System.in);
        ArrayList<Order> orders = new ArrayList<>();

        while (true) {
            showMenu();
            int itemId = -1;
            int orderId = -1;
            System.out.print("> ");
            int option = Integer.valueOf(scanner.nextLine());
            Order yourOrder = null;
            if (option == 0) {
                System.out.println("Exiting...");
                System.exit(0);
            }
            else if (option == 1 && Order.underLimit()) {
                yourOrder = new Order();
                orders.add(yourOrder);
                System.out.println("Created order with id: " + yourOrder.getId());
            }
            else if (option == 2) {
                orderId = askOrderId(scanner);
                yourOrder = orders.get(orderId - 1);
                yourOrder.addMedia(askItemBasicInfo(scanner));
            }
            else if (option == 3) {
                orderId = askOrderId(scanner);
                yourOrder = orders.get(orderId - 1);

                itemId = askItemId(scanner);
                if (yourOrder.hasItemId(itemId)) {
                    yourOrder.removeMedia(itemId);
                }
            }
            else if (option == 4) {
                orderId = askOrderId(scanner);
                orders.get(orderId - 1).printOrder();
            }
            System.out.println();
        }
    }

    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Print the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please enter a number: 0-1-2-3-4");
    }

    public static Media askItemBasicInfo(Scanner scanner) {
        System.out.print("Enter title: ");
        String title = scanner.nextLine();
        System.out.print("Enter category: ");
        String category = scanner.nextLine();
        System.out.print("Enter cost: ");
        float cost = Float.valueOf(scanner.nextLine());
        System.out.print("Enter type: ");
        String type = scanner.nextLine();

        Media item = null;
        if (type.equalsIgnoreCase("Book"))
            item = new Book(title, category, cost);
        else if (type.equalsIgnoreCase("CD")) {
            System.out.print("Enter artist: ");
            String artist = scanner.nextLine();
            ArrayList<Track> tracks = askTracksInfo(scanner);
            item = new CompactDisc(title, category, artist, tracks, cost);
        }
        else if (type.equalsIgnoreCase("DVD"))
            item = new DigitalVideoDisc(title, category, cost);
        else {
            System.out.println("Invalid! Assume it's Book!");
            item = new Book(title, category, cost);
        }

        return item;
    }

    public static ArrayList<Track> askTracksInfo(Scanner scanner) {
        ArrayList<Track> tracks = new ArrayList<>();

        System.out.print("Enter quantity: ");
        int quantity = Integer.valueOf(scanner.nextLine());

        // Prettify outputs
        int maxLength = String.valueOf(quantity).length();
        String format = "%0" + maxLength + "d";

        for (int i = 0; i < quantity; i++) {
            System.out.print("(" + String.format(format, i + 1) + ") " + "Enter title: ");
            String title = scanner.nextLine();
            System.out.print("(" + String.format(format, i + 1) + ") " + "Enter length: ");
            int length = Integer.valueOf(scanner.nextLine());
            tracks.add(new Track(title, length));
            System.out.print("(" + String.format(format, i + 1) + ") " + "Play it? [y/N]: ");
            String answer = scanner.nextLine();
            if (answer.equalsIgnoreCase("y"))
                tracks.get(i).play();
        }

        return tracks;
    }

    public static int askOrderId(Scanner scanner) {
        System.out.print("Enter order id: ");
        return Integer.valueOf(scanner.nextLine());
    }

    public static int askItemId(Scanner scanner) {
        System.out.print("Enter item id: ");
        return Integer.valueOf(scanner.nextLine());
    }
}
