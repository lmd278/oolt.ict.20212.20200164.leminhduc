package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class AimsTest {
    public static void main(String[] args) {
        Order anOrder = new Order();

        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 87, 19.95f);
        anOrder.addMedia(dvd1);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f);
        anOrder.addMedia(dvd2);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f);
        anOrder.addMedia(dvd3);

        System.out.print("Total Cost is: ");
        System.out.println(anOrder.totalCost());

        DigitalVideoDisc[] dvdList1 = new DigitalVideoDisc[3];
        dvdList1[0] = new DigitalVideoDisc("L1_0", "Test", "Test", 120, 25.00f);
        dvdList1[1] = new DigitalVideoDisc("L1_1", "Test", "Test", 120, 25.00f);
        dvdList1[2] = new DigitalVideoDisc("L1_2", "Test", "Test", 120, 25.00f);
        anOrder.addMedia(dvdList1);
        // anOrder.addMedia(dvdList[0], dvdList[1], dvdList[2]);

        anOrder.printOrder();

        DigitalVideoDisc[] dvdList2 = new DigitalVideoDisc[7];
        for (int i = 0; i < dvdList2.length; i++) {
            dvdList2[i] = new DigitalVideoDisc("L2_" + i, "Test", "Test", 120, 25.00f);
        }
        anOrder.addMedia(dvdList2[0], dvdList2[1]);  // qtyOrdered = 8
        anOrder.addMedia(dvdList2[2], dvdList2[3]);  // qtyOrdered = 10
        anOrder.removeMedia(dvdList2[3]);            // qtyOrdered = 9
        anOrder.addMedia(dvdList2[4], dvdList2[5]);  // qtyOrdered = 10
        anOrder.addMedia(dvdList2[6]);               // qtyOrdered = 10

        System.out.print("Total Cost is: ");
        System.out.println(anOrder.totalCost());
    }
}