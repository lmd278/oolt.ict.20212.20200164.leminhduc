package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.test.utils.MyDate;
import java.util.ArrayList;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;
    public static int nbOrders = 0;

    private int id;
    private MyDate dateOrdered;
    private ArrayList<Media> itemsOrdered;
    private int nbItems;

    public Order() {
        if (Order.underLimit()) {
            itemsOrdered = new ArrayList<>();
            nbItems = 0;
            dateOrdered = new MyDate();
            nbOrders++;
            id = nbOrders;
        } else {
            System.out.println("Order limit reached!");
            nbOrders++;
        }
    }

    public void addMedia(Media item) {
        if (itemsOrdered.size() + 1 <= MAX_NUMBER_ORDERED) {
            if (itemsOrdered.contains(item)) {
                System.out.println("The item is already in the order!");
            } else {
                nbItems++;
                item.setId(nbItems);
                itemsOrdered.add(item);
            }
        } else {
            System.out.println("The order is already full!");
        }
    }

    // public void addMedia(Media[] itemList) {
    public void addMedia(Media... itemList) {
        if (itemsOrdered.size() + itemList.length <= MAX_NUMBER_ORDERED) {
            for (Media item : itemList) {
                this.addMedia(item);
            }
        } else {
            System.out.println("Not enough remaining slots to add this list!");
        }
    }

    public void addMedia(Media item1, Media item2) {
        if (itemsOrdered.size() + 2 <= MAX_NUMBER_ORDERED) {
            this.addMedia(item1);
            this.addMedia(item2);
        } else if (itemsOrdered.size() + 1 <= MAX_NUMBER_ORDERED) {
            this.addMedia(item1);
            System.out.println("The order is already full! Cannot add item2!");
        } else {
            System.out.println("The order is already full! Cannot add item1 and item2!");
        }
    }

    public boolean hasItemId(int itemId) {
        for (Media item : itemsOrdered) {
            if (item.getId() == itemId)
                return true;
        }

        return false;
    }

    public void removeMedia(Media item) {
        if (itemsOrdered.contains(item)) {
            itemsOrdered.remove(item);
        }
    }

    public void removeMedia(int itemId) {
        for (Media item : itemsOrdered) {
            if (item.getId() == itemId) {
                itemsOrdered.remove(item);
                return;
            }
        }
    }

    public Media getALuckyItem() {
        int min = 0;
        int max = itemsOrdered.size();
        int luckyIndex = (int) (Math.random() * (max - min)) + min;
        return itemsOrdered.remove(luckyIndex);
    }

    public float totalCost() {
        float sum = 0;
        for (Media item : itemsOrdered) {
            sum += item.getCost();
        }

        return sum;
    }
    public void printOrder() {
        System.out.println("***********************Order***********************");
        System.out.print("Date: ");
        dateOrdered.print();
        System.out.println("Ordered Items:");

        // Prettify outputs
        int maxLength = String.valueOf(nbItems).length();
        String format = "%0" + maxLength + "d";

        for (int i = 0; i < itemsOrdered.size(); i++) {
            Media item = itemsOrdered.get(i);
            if (item instanceof Book)
                System.out.println(String.format(format, item.getId()) + ". bk. - " + item);
            else if (item instanceof CompactDisc)
                System.out.println(String.format(format, item.getId()) + ". CD - " + item);
            else if (item instanceof DigitalVideoDisc)
                System.out.println(String.format(format, item.getId()) + ". DVD - " + item);
            else
                System.out.println(String.format(format, item.getId()) + ". ? - " + item);
        }

        System.out.println("Total cost: " + this.totalCost());
        System.out.println("***************************************************");
    }

    public static boolean underLimit() {
        return nbOrders < MAX_LIMITED_ORDERS;
    }

    public MyDate getDateOrdered() {
        return dateOrdered;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
